/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pong2;

import java.util.Random;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 *
 * @author Antonio
 */
public class Pong2 extends Application {
    //variables para el mundo
    final int WORLD_WIDTH = 500;
    final int WORLD_HEIGHT = 400;
    final int START_WORLD_WIDTH = 0;
    final int START_WORLD_HEIGHT = 0;
    //variable para la bolita
    final double CENTRO_X = 0;
    final double CENTRO_Y = 0;
    final double RADIO = 10;
    final int VELOCIDAD_BOLITA_X = 2;
    final int VELOCIDAD_INICIAL_BOLITA_Y = 2;
    final int VELOCIDAD_MEDIA = 4;
    final int VELOCIDAD_RAPIDA = 6;
    int velocidadActualBolitaY = 1;
    //variables para las palas  
    final double PALA_IZQUIERDA_POSICION_X = 0;     
    final double PALA_IZQUIERDA_POSICION_Y = 0;
    final double DISTANCIA_HASTA_LA_PALA = 25;
    final double ANCHO_PALA = 5;
    final double ALTO_PALA = 50;
    final double PALA_DERECHA_POSICION_X = 0;
    final double PALA_DERECHA_POSICION_Y = 0;
    boolean gravity;                            //false -5 y true 5
    double gravityDerechaY = gravity? 5 : -5;
    double gravityIzquierdaY = 5;  
    int pararPala = 0;
    int numeroDeTrozoDePalaIzquierda;
    //variables para los marcadores
    int puntuacion1 = 0;
    int puntuacion2 = 0;
    final int FIN_DE_PARTIDA = 5;
    //direccion de comienzo e la bolita
    boolean direccionIzquierdaDerecha;   //direccion true Izquierda false Derecha
    boolean direccionArribaAbajo;       //direccion true Abajo false Arriba
    //monedas
    int moneda;    
    int unaMoneda = 1;
    int monedaActual = 0;
    //principal
    AnimationTimer comienzo;
    
    //creacion de objetos
    //para la direccion de la primera bola
    Random generadorNumerosAleatorios = new Random();
    //linea del campo
    Rectangle tamañoMundo = new Rectangle(START_WORLD_WIDTH, START_WORLD_HEIGHT,
            WORLD_WIDTH, WORLD_HEIGHT);
    //creacion de la bola
    Circle bolita = new Circle(CENTRO_X, CENTRO_Y, RADIO);
    //pala izquierda
    Rectangle palaIzquierda = new Rectangle(PALA_IZQUIERDA_POSICION_X,
            PALA_IZQUIERDA_POSICION_Y, ANCHO_PALA, ALTO_PALA);
    //pala izquierda
    Rectangle palaDerecha = new Rectangle(PALA_DERECHA_POSICION_X,
            PALA_DERECHA_POSICION_Y, ANCHO_PALA, ALTO_PALA);
    //la linea de centro del campo
    Line lineaDeCentro = new Line(WORLD_WIDTH * 0.5, START_WORLD_HEIGHT,
            WORLD_WIDTH * 0.5, WORLD_HEIGHT);
    //marcadores
    //el Marcador 1
    Text marcador1 = new Text(WORLD_WIDTH * 0.25, START_WORLD_HEIGHT + 20,
            String.valueOf(puntuacion1));
    //el Marcador 2
    Text marcador2 = new Text(WORLD_WIDTH * 0.75, START_WORLD_HEIGHT + 20,
            String.valueOf(puntuacion2));
    //Ganador
    //el jugador 1
    Text jugador1 = new Text(WORLD_WIDTH * 0.3, WORLD_HEIGHT * 0.25,
            String.valueOf("The winner is:    jugador 1"));
    //el jugador 2
    Text jugador2 = new Text(WORLD_WIDTH * 0.3, WORLD_HEIGHT * 0.25,
            String.valueOf("The winner is:    jugador 2"));
    //boton de reinicio de partida
    Button reiniciarPartida = new Button("Reiniciar partida");
    //el contador para las monedas
    Label monedas = new Label("Monedas introducidas: " 
            + String.valueOf(monedaActual) + '\n'
            + "Introduce 1 moneda con Z o 2 con X");
    
    
    
    @Override
    public void start(Stage primaryStage) {
        // Create scene
        Group root = new Group();
        Scene scene = new Scene(root, WORLD_WIDTH, WORLD_HEIGHT + 100, Color.BLACK);
        primaryStage.setTitle("Pong");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        //tamaño mundo
        tamañoMundo.setStroke(Color.WHITE);
        root.getChildren().add(tamañoMundo);
        //bolita
        bolita.setFill(Color.WHITE);
        root.getChildren().add(bolita);
        bolita.setTranslateX(WORLD_WIDTH * 0.5);
        bolita.setTranslateY(WORLD_HEIGHT * 0.5);
        //pala izquierda
        palaIzquierda.setFill(Color.WHITE);
        root.getChildren().add(palaIzquierda); 
        palaIzquierda.setTranslateX(START_WORLD_WIDTH + DISTANCIA_HASTA_LA_PALA);
        palaIzquierda.setTranslateY(WORLD_HEIGHT * 0.5 - ALTO_PALA * 0.5);      
        //pala derecha
        palaDerecha.setFill(Color.WHITE);
        root.getChildren().add(palaDerecha); 
        palaDerecha.setTranslateX(WORLD_WIDTH - ANCHO_PALA - DISTANCIA_HASTA_LA_PALA);
        palaDerecha.setTranslateY(WORLD_HEIGHT * 0.5 - ALTO_PALA * 0.5);
        //linea de centro del campo
        lineaDeCentro.setStroke(Color.WHITE);
        root.getChildren().add(lineaDeCentro);
        //marcadores
        //marcador 1
        marcador1.setStroke(Color.WHITE);
        root.getChildren().add(marcador1);  
        marcador1.setScaleX(2);
        marcador1.setScaleY(2);
        //marcador 2
        marcador2.setStroke(Color.WHITE);
        root.getChildren().add(marcador2);
        marcador2.setScaleX(2);
        marcador2.setScaleY(2);
        //Monedas
        monedas.setTextFill(Color.WHITE);
        root.getChildren().add(monedas);
        monedas.setFont(new Font(20));
        monedas.setTranslateX(WORLD_WIDTH * 0.10);
        monedas.setTranslateY(WORLD_HEIGHT + 20);
        //reiniciar partida
        root.getChildren().add(reiniciarPartida);
        reiniciarPartida.setVisible(false);
        //ganador
        //jugador1
        root.getChildren().add(jugador1);
        jugador1.setVisible(false);
        //jugador2
        root.getChildren().add(jugador2);
        jugador2.setVisible(false);

        //para que lado comienza la primera bolita
        direccionIzquierdaDerecha = generadorNumerosAleatorios.nextBoolean();
        direccionArribaAbajo = generadorNumerosAleatorios.nextBoolean();
        //parar las palas en la primera partida
        gravityIzquierdaY = pararPala;
        gravityDerechaY = pararPala;
        
        comienzo = new AnimationTimer() {
            @Override
            public void handle(long now) {   
                if(monedaActual >= unaMoneda){ 
                    //variables de posicion
                    bolita.getTranslateX();
                    bolita.getTranslateY(); 

                    //direccion de la bola
                    if(direccionIzquierdaDerecha == true){
                       bolita.setTranslateX(bolita.getTranslateX()
                               - VELOCIDAD_BOLITA_X);                  //izquierda
                        //rebote de la bola en la pala
                        if ((palaIzquierda.getTranslateY() <= bolita.getTranslateY() - RADIO 
                                && palaIzquierda.getTranslateY() + ALTO_PALA >= bolita.getTranslateY() + RADIO) 
                                && (palaIzquierda.getTranslateX() + ANCHO_PALA == bolita.getTranslateX() - RADIO)){
                            direccionIzquierdaDerecha = false;
                            //rebote de la bola en distintos sitios de la pala
                            numeroDeTrozoDePalaIzquierda = (int)((bolita.getTranslateY()
                                    - palaIzquierda.getTranslateY()) * 0.1); //entre 10
                            switch(numeroDeTrozoDePalaIzquierda){
                                case 0:{
                                    velocidadActualBolitaY = VELOCIDAD_RAPIDA;
                                    break;
                                }
                                case 1:{
                                    velocidadActualBolitaY = VELOCIDAD_MEDIA;
                                    break;
                                }
                                case 2:{
                                    velocidadActualBolitaY = VELOCIDAD_INICIAL_BOLITA_Y;
                                    break;
                                }
                                case 3:{
                                    velocidadActualBolitaY = VELOCIDAD_MEDIA;
                                    break;
                                }
                                case 4:{
                                    velocidadActualBolitaY = VELOCIDAD_RAPIDA;
                                    break;
                                }      
                            }
                            //direccion vertical de la bola
                            if(palaIzquierda.getTranslateY() <= bolita.getTranslateY() 
                                    && palaIzquierda.getTranslateY() + ALTO_PALA * 0.5 >= bolita.getTranslateY()){
                                direccionArribaAbajo = false;
                            }
                            else{
                                direccionArribaAbajo = true;
                            }  
                        }
                        //puntuacion
                        if(bolita.getTranslateX() == START_WORLD_WIDTH + RADIO){
                            puntuacion2++;
                            direccionIzquierdaDerecha = false;
                            direccionArribaAbajo = generadorNumerosAleatorios.nextBoolean();
                            bolita.setTranslateX(WORLD_WIDTH * 0.5);
                            bolita.setTranslateY(WORLD_HEIGHT * 0.5);
                            velocidadActualBolitaY = VELOCIDAD_INICIAL_BOLITA_Y;
                        }
                        //revote vertical de la bola
                        if(direccionArribaAbajo == false){
                            bolita.setTranslateY(bolita.getTranslateY() - velocidadActualBolitaY);                               //arriba
                            if(bolita.getTranslateY() <= START_WORLD_HEIGHT + RADIO){
                                direccionArribaAbajo = true;
                            }
                        }
                        else{
                            bolita.setTranslateY(bolita.getTranslateY() + velocidadActualBolitaY);                               //abajo
                            if(bolita.getTranslateY() >= WORLD_HEIGHT - RADIO){
                                direccionArribaAbajo = false;
                            }
                        }
                    }
                    //direccion de la bola
                    else{
                        bolita.setTranslateX(bolita.getTranslateX() + VELOCIDAD_BOLITA_X);                     //derecha
                        //rebote de la bola en la pala
                        if ((palaDerecha.getTranslateY() <= bolita.getTranslateY() 
                                && palaDerecha.getTranslateY() + ALTO_PALA >= bolita.getTranslateY())
                                && (palaDerecha.getTranslateX() == bolita.getTranslateX() + RADIO)){
                            direccionIzquierdaDerecha = true; 
                            //rebote de la bola en distintos sitios de la pala
                            numeroDeTrozoDePalaIzquierda = (int)((bolita.getTranslateY() 
                                    - palaIzquierda.getTranslateY())*0.1);
                            switch(numeroDeTrozoDePalaIzquierda){
                                case 0:{
                                    velocidadActualBolitaY = VELOCIDAD_RAPIDA;
                                    break;
                                }
                                case 1:{
                                    velocidadActualBolitaY = VELOCIDAD_MEDIA;
                                    break;
                                }
                                case 2:{
                                    velocidadActualBolitaY = VELOCIDAD_INICIAL_BOLITA_Y;
                                    break;
                                }
                                case 3:{
                                    velocidadActualBolitaY = VELOCIDAD_MEDIA;
                                    break;
                                }
                                case 4:{
                                    velocidadActualBolitaY = VELOCIDAD_RAPIDA;
                                    break;
                                }      
                            }
                            if(palaDerecha.getTranslateY() <= bolita.getTranslateY() 
                                    && palaDerecha.getTranslateY() + ALTO_PALA >= bolita.getTranslateY()){
                                direccionArribaAbajo = false;
                            }
                            else{
                                direccionArribaAbajo = true;
                            }
                        }
                        //puntuacion
                        if(bolita.getTranslateX()== WORLD_WIDTH - RADIO){       
                            puntuacion1++;
                            direccionIzquierdaDerecha = true;
                            direccionArribaAbajo = generadorNumerosAleatorios.nextBoolean();
                            bolita.setTranslateX(WORLD_WIDTH * 0.5);
                            bolita.setTranslateY(WORLD_HEIGHT * 0.5);
                            velocidadActualBolitaY = VELOCIDAD_INICIAL_BOLITA_Y;
                        }  
                        //rebote vertical de la bola
                        if(direccionArribaAbajo == true){
                            bolita.setTranslateY(bolita.getTranslateY() + velocidadActualBolitaY);                               //abajo
                            if(bolita.getTranslateY() >= WORLD_HEIGHT - RADIO){
                                direccionArribaAbajo = false;
                            }
                        }
                        else{
                            bolita.setTranslateY(bolita.getTranslateY() - velocidadActualBolitaY);                               //arriba
                            if(bolita.getTranslateY() <= START_WORLD_HEIGHT + RADIO){
                                direccionArribaAbajo = true;
                            }
                        }
                    }

                    //cambiar la bola de posicion
                    bolita.setTranslateX(bolita.getTranslateX());
                    bolita.setTranslateY(bolita.getTranslateY()); 
                    //cambiar el marcador                
                    marcador1.setText(String.valueOf(puntuacion1));
                    marcador2.setText(String.valueOf(puntuacion2));
                    //ganador de la partida
                    if(puntuacion1 == FIN_DE_PARTIDA || puntuacion2 == FIN_DE_PARTIDA){
                        this.stop();
                        if(puntuacion1 == FIN_DE_PARTIDA){
                        //JUGADOR_1
                            jugador1.setVisible(true);
                            jugador1.setTextAlignment(TextAlignment.LEFT);
                            jugador1.setScaleX(3);
                            jugador1.setScaleY(3);
                            jugador1.setStroke(Color.WHITE);
                            
                        }
                        else{
                        //JUGADOR_2
                            jugador2.setVisible(true);
                            jugador2.setTextAlignment(TextAlignment.LEFT);
                            jugador2.setScaleX(3);
                            jugador2.setScaleY(3);
                            jugador2.setStroke(Color.WHITE);
                        }
                        
                        if(monedaActual >= unaMoneda){
                            monedaActual--;
                            //boton de reinicio de partida
                            reiniciarPartida.setVisible(true);
                            reiniciarPartida.setTranslateX(WORLD_WIDTH * 0.7);
                            reiniciarPartida.setTranslateY(WORLD_HEIGHT + 20);
                            monedas.setText("Monedas introducidas: " + String.valueOf(monedaActual) 
                                    + '\n' + "Introduce 1 moneda con Z o 2 con X");
                            monedas.setTranslateX(WORLD_WIDTH * 0.10);
                            monedas.setTranslateY(WORLD_HEIGHT + 20);

                            reiniciarPartida.setOnAction(new EventHandler<ActionEvent>() {
                                @Override public void handle(ActionEvent e) {
                                    reiniciarPartida.setVisible(false);
                                    puntuacion1 = 0;
                                    puntuacion2 = 0;
                                    marcador1.setText(String.valueOf(puntuacion1));
                                    marcador2.setText(String.valueOf(puntuacion2));
                                    jugador1.setVisible(false);
                                    jugador2.setVisible(false);
                                    palaIzquierda.setTranslateX(START_WORLD_WIDTH + DISTANCIA_HASTA_LA_PALA);
                                    palaIzquierda.setTranslateY(WORLD_HEIGHT * 0.5 - ALTO_PALA * 0.5);
                                    gravityIzquierdaY = pararPala;
                                    palaDerecha.setTranslateX(WORLD_WIDTH-ANCHO_PALA - DISTANCIA_HASTA_LA_PALA);
                                    palaDerecha.setTranslateY(WORLD_HEIGHT * 0.5 - ALTO_PALA * 0.5);
                                    gravityDerechaY = pararPala;
                                    comienzo.start();
                                }
                            });
                        }
                    }

                    //movimiento pala izquierda
                    if(gravityIzquierdaY < pararPala){
                        palaIzquierda.setTranslateY(palaIzquierda.getTranslateY() 
                                + gravityIzquierdaY);
                        if(palaIzquierda.getTranslateY() == START_WORLD_HEIGHT){
                           gravityIzquierdaY = pararPala; 
                        } 
                    }
                    else{
                        if(gravityIzquierdaY > pararPala){
                            palaIzquierda.setTranslateY(palaIzquierda.getTranslateY() + gravityIzquierdaY);
                            if(palaIzquierda.getTranslateY() == WORLD_HEIGHT - ALTO_PALA){
                                gravityIzquierdaY = pararPala; 
                            } 
                        }
                    }
                    //movimiento pala derecha
                    if(gravityDerechaY < pararPala){
                        palaDerecha.setTranslateY(palaDerecha.getTranslateY() + gravityDerechaY);
                        if(palaDerecha.getTranslateY() == START_WORLD_HEIGHT){
                           gravityDerechaY = pararPala; 
                        } 
                    }
                    else{
                        if(gravityDerechaY > pararPala){
                            palaDerecha.setTranslateY(palaDerecha.getTranslateY() + gravityDerechaY);
                            if(palaDerecha.getTranslateY() == WORLD_HEIGHT - ALTO_PALA){
                                gravityDerechaY = pararPala; 
                            } 
                        }
                    }
                }  
            }      
        };
        comienzo.start();


        scene.setOnKeyPressed(new EventHandler<KeyEvent>(){
                    @Override
                    public void handle(KeyEvent event) {
                        switch(event.getCode())    {
                            case W:{
                                if(palaIzquierda.getTranslateY() > START_WORLD_HEIGHT){
                                    gravity = false;
                                    gravityIzquierdaY = gravity? 5 : -5;
                                }
                                break;
                            }
                            case S:{
                                if(palaIzquierda.getTranslateY() < WORLD_HEIGHT-ALTO_PALA){
                                    gravity = true;
                                    gravityIzquierdaY = gravity? 5 : -5;
                                }
                                break;
                            }
                            case UP:{
                                if(palaDerecha.getTranslateY() > START_WORLD_HEIGHT){
                                    gravity = false;
                                    gravityDerechaY = gravity? 5 : -5;
                                }
                                break;
                            }
                            case DOWN:{
                                if(palaDerecha.getTranslateY() < WORLD_HEIGHT-ALTO_PALA){
                                    gravity = true;
                                    gravityDerechaY = gravity? 5 : -5;
                                }
                                break;
                            }
                            case Z:{
                                moneda = unaMoneda;
                                monedaActual += moneda;
                                monedas.setText("Monedas introducidas: " + String.valueOf(monedaActual) 
                                        + '\n' + "Introduce 1 moneda con Z o 2 con X");
                                break;
                            }
                            case X:{
                                moneda = unaMoneda * 2;
                                monedaActual += moneda;
                                monedas.setText("Monedas introducidas: " + String.valueOf(monedaActual) 
                                        + '\n' + "Introduce 1 moneda con Z o 2 con X");
                                break;
                            }  
                            default:{
                                moneda = unaMoneda * 0;
                                break;
                            }
                        }
                    }
                });

    }
   public static void main(String[] args) {
        launch(args);
    }
}